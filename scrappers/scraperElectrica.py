from bs4 import BeautifulSoup
import requests
import unidecode

URL = "http://um.edu.ar/es/fi/carreras/ingenier%C3%ADa-el%C3%A9ctrica.html"

questions = []
responses = []

# Realizamos la petición a la web
req = requests.get(URL)

# Comprobamos que la petición nos devuelve un Status Code = 200
status_code = req.status_code

# Si trae los datos del html realiza las tareas del scrapper
if status_code == 200:

    # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
    html = BeautifulSoup(req.text, "html.parser")
    # Obtenemos todos los divs donde están las entradas
    entradas = html.find_all('div', {'class': 'contenido'})
    # Obtenemos todos los parrafos
    content = html.find_all('p')
    # Obtenemos todos los header de h2
    titles1 = html.find_all('h2')
    # Obtenemos todos los header de h3
    titles2 = html.find_all('h3')

    # Se itera para cada parrafo
    for p in content:
        # Si se encuentra la palabra duracion en el parrafo se realiza lo de abajo
        if "duracion" in p.prettify().lower() or "duración" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()

            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text[text.index("Duración"):].replace(':', ''))

            # Se añade la pregunta a la cual se debe responder
            questions.append("duracion ingenieria")
            responses.append(formatted_text)
            questions.append("dura ingenieria")
            responses.append(formatted_text)
            questions.append("Cuanto dura ingenieria?")
            responses.append(formatted_text)
            questions.append("Cuanto dura ingenieria Electrica?")
            responses.append(formatted_text)
            questions.append("Cuantos anos dura la carrera ingenieria electrica?")
            responses.append(formatted_text)

        if "Alcance" in p.prettify().lower() or "Alcance" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a alcance, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text[text.index("Alcance"):].replace(':', ''))

            # Se añade la pregunta a la cual se debe responder
            questions.append("alcance del titulo?")
            responses.append(formatted_text)
            questions.append("cual es el alcance del titulo?")
            responses.append(formatted_text)

        if "Actividades" in p.prettify().lower() or "Actividades" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a alcance, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text[text.index("Actividades"):].replace(':', ''))

            # Se añade la pregunta a la cual se debe responder
            questions.append("que actividades me permite este titulo?")
            responses.append(formatted_text)
            questions.append("que actividades puedo hacer con esta carrera?")
            responses.append(formatted_text)


    # Se abre el archivo en modo escritura
    file = open("./data/electrica.yml", "w+")
    # Se escriben las primeras lineas del yaml necesarias para el ChatBot
    file.write("categories: \n- general \nconversations: \n")

    # Para cada pregunta se asocia una respuesta
    for i in range(len(responses)):
        file.write("- - " + questions[i] + "\n")
        file.write("  - " + responses[i] + "\n")

    print("Scraper de Electrica terminado con exito")

# En caso de que la pagina no devuelva un codigo 200 (Exito) se va a el error
else:
    print("Status Code %d" % status_code)
